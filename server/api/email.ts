import { Resend } from "resend";

const resend = new Resend(process.env.RESEND_API);

export default defineEventHandler(async (event) => {
  const { name, message, email, phone } = await readBody(event);
  const date = new Date();
  try {
    const data = await resend.emails.send({
      from: "monadam.xyz <onboarding@resend.dev>",
      to: [process.env.MY_EMAIL],
      subject: `[monadam.xyz] Message from ${name}`,
      html: `
<table>
    <tr>
        <td>Name</td>    <td>${name}</td>
    </tr>
    <tr>
        <td>Email</td>   <td>${email}</td>
    </tr>
    <tr>
        <td>Phone</td>   <td>${phone || "none"}</td>
    </tr>
    <tr>
        <td>Message</td> <td>${message}</td>
    </tr>
    <tr>
        <td>Date</td>    <td>${date}</td>
    </tr>
</table>
`,
    });

    return data;
  } catch (error) {
    return { error };
  }
});
